[paramslist,strlist] = getparamsandstruct;
N = 256; %number of pixel NxN
fovlist = [15 20]; %in angstrom field of view %30, 60, 90
anglelist = [0:15:90]; %rotation angles
count = 1;
backgrounddir = 'D:\background';
exportdir = 'D:\deep_learning\atomsegmentation';
figure;
mkdir([exportdir '\image']);
mkdir([exportdir '\mask']);
for m = 1:length(fovlist)
    fov = fovlist(m);
    for i = 1:4
        for j = 1:length(strlist(i).name)
            for k = 1:length(anglelist)
                angle = anglelist(k); %rotation angle
                params = paramslist(i); %microscope conditions
                structname = strlist(i).name{j};  %structure
                [II, M] = genimage(structname,params,fov,N,angle);
                L = M.xlen;
                dx = L/N;
                %image with scan noise
                IIshot = imAddShotNoise(II*0.1,params.dwelltime,params.current);
                %image with scan noise
                IIscan = imAddScanNoise(IIshot,params.xgitter/dx,params.ygitter/dx,params.dwelltime,0);
                %convert to pixel coordiates. These are where the atoms are.
                xpos = M.xpos/L*N + 1;
                ypos = M.ypos/L*N + 1;
                
                radius = sqrt((0.25*electronwavelength(params.kev)/(params.amax/1000)).^2 + (params.source/2).^2);
                radius = radius/(L/N);
                IImask = circularmask(radius,[xpos',ypos'],N,N);
                
                %now add a randomly picke background
                sel = ceil(rand(1)*252);
                filename = fullfile(backgrounddir,['bg_' numstr_convert(sel,4) '.mat']);
                load(filename);
                bg = imresize(bg,size(II));
                II1 = IIscan./totmax(IIscan)+2*bg;
                II2 = IIscan./totmax(IIscan)+0.8*bg;
                II3 = IIscan./totmax(IIscan)+0.5*bg;
                
                %now add a linear ramp
                %scalefactor = rand(2)*2;
                scalefactor = [1 1];
                [bg_x,bg_y] = meshgrid(linspace(0,1,N),linspace(0,1,N));
                II4 = IIscan./totmax(IIscan) + bg_x*scalefactor(1);
                II5 = IIscan./totmax(IIscan) + bg_y*scalefactor(2);
               
                
                imwrite(dbl28(II),fullfile([exportdir '\image'],[numstr_convert(count,5) '.png']));
                imwrite(uint8(IImask),fullfile([exportdir '\mask'],[numstr_convert(count,5) '.png']));
                count = count + 1;
                imwrite(dbl28(IIshot),fullfile([exportdir '\image'],[numstr_convert(count,5) '.png']));
                imwrite(uint8(IImask),fullfile([exportdir '\mask'],[numstr_convert(count,5) '.png']));
                count = count + 1;
                imwrite(dbl28(IIscan),fullfile([exportdir '\image'],[numstr_convert(count,5) '.png']));
                imwrite(uint8(IImask),fullfile([exportdir '\mask'],[numstr_convert(count,5) '.png']));
                count = count + 1;
                imwrite(dbl28(II1),fullfile([exportdir '\image'],[numstr_convert(count,5) '.png']));
                imwrite(uint8(IImask),fullfile([exportdir '\mask'],[numstr_convert(count,5) '.png']));
                count = count + 1;
                imwrite(dbl28(II2),fullfile([exportdir '\image'],[numstr_convert(count,5) '.png']));
                imwrite(uint8(IImask),fullfile([exportdir '\mask'],[numstr_convert(count,5) '.png']));
                count = count + 1;
                imwrite(dbl28(II3),fullfile([exportdir '\image'],[numstr_convert(count,5) '.png']));
                imwrite(uint8(IImask),fullfile([exportdir '\mask'],[numstr_convert(count,5) '.png']));
                count = count + 1;
                imwrite(dbl28(II4),fullfile([exportdir '\image'],[numstr_convert(count,5) '.png']));
                imwrite(uint8(IImask),fullfile([exportdir '\mask'],[numstr_convert(count,5) '.png']));
                count = count + 1;
                imwrite(dbl28(II5),fullfile([exportdir '\image'],[numstr_convert(count,5) '.png']));
                imwrite(uint8(IImask),fullfile([exportdir '\mask'],[numstr_convert(count,5) '.png']));
                count = count + 1;
                
                
                %             subplot(2,3,1)
                %             imagesc(II); axis image; colormap(gray)
                %             hold on;
                %             plot(ypos,xpos,'r.');
                %             hold off;
                %             subplot(2,3,2);
                %             imagesc(IIshot); axis image; colormap(gray);
                %             subplot(2,3,3);
                %             imagesc(IIscan); axis image; colormap(gray);
                %             subplot(2,3,4);
                %             imagesc(IImask); axis image; colormap(gray);
                %             hold on;
                %             plot(ypos,xpos,'r.');
                %             hold off;
                %             drawnow;
                %
                
            end
        end
    end
end
