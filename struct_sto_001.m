function M = sto001xyz(nx,ny,varargin)
% M = sto001xyz(nx,ny,nz,varargin)
% nx,ny,nz number of unit cells along each coordinates
% varargin{1} = tdsornot : include phonon or not

nz = 1;
if ~isempty(varargin)
    tdsornot = varargin{1};
else
    tdsornot = 0;
end

if tdsornot
    M.head = ['STO [001]' num2str(nx) 'x' num2str(ny) 'x' num2str(nz) ' with TDS'];
else
    M.head = ['STO [001]' num2str(nx) 'x' num2str(ny) 'x' num2str(nz)];
end

acell = 3.905;

s = [38,0.,0.,0.];
t = [22,1/2,1/2,1/2];

pos = [s;t;];

[numat temp] = size(pos);

M.xlen = acell*nx;
M.ylen = acell*ny;
M.zlen = acell*nz;

N = nx*ny*nz*numat;

M.Znum = zeros(1,N);
M.xpos = zeros(1,N);
M.ypos = zeros(1,N);
M.zpos = zeros(1,N);
M.wt = zeros(1,N);
M.tds = zeros(1,N);

for i=0:nx-1
    for j = 0:ny-1
        for k = 0:nz-1
            for m = 1:numat
                n = m + numat*k + numat*nz*j + numat*nz*ny*i;

                M.Znum(n) = pos(m,1);
                M.xpos(n) = pos(m,2)*acell+i*acell;
                M.ypos(n) = pos(m,3)*acell+j*acell;
                M.zpos(n) = pos(m,4)*acell+k*acell;
                M.wt(n) = 1;
                if tdsornot == 0
                    M.tds(n) = 0;
                else
                    if M.Znum(n)==38
                        M.tds(n) = 0.0887;
                    elseif M.Znum(n)==22
                        M.tds(n) = 0.0746;
                    elseif M.Znum(n)==8
                        M.tds(n) = 0.0963;
                    else
                        error(['abnormal Z number! Z = ', num2str(M.Znum(n))]);
                    end
                end
            end
        end
    end
end