function M = graphitexyz(nx,ny)
%here nz is the number of graphene layers
%by Huolin Xin

nz = 1;
a = 2.456;
b = 4.254;
c = 6.696;

basis1 = [0 b/6 0;0 b*5/6 0; a/2 b/3 0; a/2 b*2/3 0];
basis2 = [0 b/6 c/2;0 b/2 c/2;a/2 b*2/3 c/2;a/2 b c/2];

M.head = ['Graphite [001] ' 'nx ' num2str(nx) ' ny ' num2str(ny) ' nz ' num2str(nz) ];
M.xpos = zeros(1,4*nx*ny*nz);
M.ypos = zeros(1,4*nx*ny*nz);
M.zpos = zeros(1,4*nx*ny*nz);
M.Znum = 6*ones(1,4*nx*ny*nz);
M.wt   = ones(1,4*nx*ny*nz);
M.tds  = zeros(1,4*nx*ny*nz);

M.xlen = a*nx;
M.ylen = b*ny;
M.zlen = c/2*nz;
num = 0;
for i=1:nz
    for j=1:nx
        for k=1:ny
            for l=1:4
                num = num+1;
                if isodd(i)
                    M.xpos(num) = basis1(l,1)+a*(j-1);
                    M.ypos(num) = basis1(l,2)+b*(k-1);
                    M.zpos(num) = basis1(l,3)+c*(i-1)/2;
                else
                    M.xpos(num) = basis2(l,1)+a*(j-1);
                    M.ypos(num) = basis2(l,2)+b*(k-1);
                    M.zpos(num) = basis2(l,3)+c*(i-2)/2;
                end
            end
        end
    end
end

function flag = isodd(x)
%function flag = isodd(x)
%by Huolin Xin

index = (mod(x,2)==0);
flag = ones(size(x));
flag(index) = 0;