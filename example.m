%setting up imaging conditions
params.C3   = 0; %don't change
params.C5   = 0; %don't change
params.C7   = 0; %don't change
params.df   = 0; %don't change
params.kev  = 200;
params.amin = 0; %don't change
params.amax = 10.5; %this can vary between 10.5 and 24
params.source = 0.8; %this can vary between 0.7 to 1.8
params.dwelltime = 16;
params.current = 30;
params.xgitter = 0.25; %angstrom
params.ygitter = params.xgitter;
N = 1024;
fov = 20;


test = struct_sto_001(1,1);
a = test.xlen;
b = test.ylen;
nx = round(fov/a);
ny = round(fov/b);
M = struct_sto_001(nx,ny);
xlen = M.xlen;
ylen = M.ylen;
L = max(xlen,ylen);
angle = 90;
rmat = rotatemat('z', angle/180*pi);
temp = rmat*[M.xpos - M.xlen/2;M.ypos - M.ylen/2;M.zpos];

for i = 1:2
    temp(i,:) = temp(i,:) + L/2;
end
ind = temp(1,:)>=0 & temp(1,:)<L & temp(2,:)>=0 & temp(2,:)<L;
M.xpos = temp(1,ind);
M.ypos = temp(2,ind);
M.zpos = temp(3,ind);
M.Znum = M.Znum(ind);
M.xlen = L;
M.ylen = L;


%generate the potential map
%M = struct_si_110(7,5); %this needs amax = 24
%M = struct_sto_001(10,10);
%M = struct_al2o3_110(10,4); %this needs amax = 24
%M = struct_rocksalt_001(6,6);
%M = struct_rocksalt_110(8,6);
%M = struct_pt_001(5,5);
%M = struct_pt_110(14,10);
%M = struct_mos2(9,5);
%M = struct_amorphousgraphene(); %this needs amax = 24;
%M = struct_dso_110(3,5);
%M = struct_sto_011(6,8);
%M = struct_graphene(8,5); %this needs amax = 24
%M = struct_nmc442_010(14,5);
%M = struct_rutile_001(5,5);
%M = struct_rutile_100(9,6);
%M = struct_rutile_110(11,5); %this needs amax = 24

V = potmap(L,L,N,N,M);
dx = L/N;

%generate the point spread function
probe = probeblur(L,N,params);
blur = gaussianblur(L,N,params.source);
ctf = fft2(probe).*fft2(blur);

%final image;
II = ifft2(fft2(V).*ctf,'symmetric');
%image with scan noise
IIshot = imAddShotNoise(II,params.dwelltime,params.current);
%image with scan noise
IIscan = imAddScanNoise(IIshot,params.xgitter/dx,params.ygitter/dx,params.dwelltime,0);

xpos = M.xpos/L*N + 1;
ypos = M.ypos/L*N + 1;

figure;
subplot(2,3,1)
imagesc(II); axis image; colormap(gray)
hold on;
plot(ypos,xpos,'r.');
subplot(2,3,2);
imagesc(IIshot); axis image; colormap(gray);
subplot(2,3,3);
imagesc(IIscan); axis image; colormap(gray);

%now add background
filename = fullfile([pwd '\background'],'bg_0009.mat');
load(filename);
II1 = IIscan./totmax(IIscan)+bg;
II2 = IIscan./totmax(IIscan)+0.5*bg;
II3 = IIscan./totmax(IIscan)+0.3*bg;

load(filename);
subplot(2,3,4);
imagesc(II1); axis image; colormap(gray);
subplot(2,3,5);
imagesc(II2); axis image; colormap(gray);
subplot(2,3,6);
imagesc(II3); axis image; colormap(gray);

imwrite(dbl28(II),'image.png');
imwrite(dbl28(IIshot),'image_shot.png');
imwrite(dbl28(IIscan),'image_scan.png');
imwrite(dbl28(II1),'image_bg1.png');
imwrite(dbl28(II2),'image_bg2.png');
imwrite(dbl28(II3),'image_bg3.png');

