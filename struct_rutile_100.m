function M = tio2_rutile_100_xyz(nx,ny)
%by Huolin Xin
nz = 1;
a =    2.959000;
b =    4.594000;
c =    4.594000;

pos =[   
            22           1         0.000000   1.000000   0.000000   
            22           2         0.500000   0.500000   0.500000   ];

numatom = size(pos,1);
M.head = ['TiO2_rutile [100] ' 'nx ' num2str(nx) ' ny ' num2str(ny) ' nz ' num2str(nz) ];
M.xpos = zeros(1,numatom*nx*ny*nz);
M.ypos = zeros(1,numatom*nx*ny*nz);
M.zpos = zeros(1,numatom*nx*ny*nz);
M.Znum = zeros(1,numatom*nx*ny*nz);
M.wt   = ones(1,numatom*nx*ny*nz);
M.tds  = zeros(1,numatom*nx*ny*nz);

M.xlen = a*nx;
M.ylen = b*ny;
M.zlen = c*nz;
num = 0;
for i=1:nz
    for j=1:nx
        for k=1:ny
            for l=1:numatom
                    num = num+1;
                    M.Znum(num) = pos(l,1);
                    M.xpos(num) = pos(l,3)*a+a*(j-1);
                    M.ypos(num) = pos(l,4)*b+b*(k-1);
                    M.zpos(num) = pos(l,5)*c+c*(i-1);
            end
        end
    end
end

function flag = isodd(x)
%function flag = isodd(x)
%by Huolin Xin

index = (mod(x,2)==0);
flag = ones(size(x));
flag(index) = 0;