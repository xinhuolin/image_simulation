file = dir('*.jpg');
figure;
div = [4 10 20];
num = 1;
gen = rand(length(file),length(div),3);
for i=1:length(file)
    rgb = imread(file(i).name);
    II = rgb2gray(rgb);
    II = double(II);
    n = min(size(II));
    n1 = size(II,1);
    n2 = size(II,2);
    for j=1:length(div)
        I =  imgaussfilt(II,ceil(n/div(j)),'padding','symmetric');
        for k = 1:size(gen,3)
            ns = ceil(gen(i,j,k)*(n2-n1));
            sub = I(:,ns:ns+n1-1);
            if mod(num,2) == 0
                sub = sub';
            end
            bg = (sub)./max(sub(:));
            bg = imresize(bg,[1024 1024]);
            save(['bg_' numstr_convert(num,4) '.mat'],'bg');
            num = num + 1;
            %imshow(bg,[]);
            %drawnow;
        end
    end
end