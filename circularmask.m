function mask = circularmask(r,pos,nx,ny)
%by Huolin Xin

dx = 1;
R = ceil(r);
vec = single(-R:dx:R);
[xx, yy] = meshgrid(vec,vec);
rr = sqrt(xx.^2+yy.^2);
cir = (rr<=r);
L = length(vec);
Lmid = find(vec == min(abs(vec)));
arr = (1:L)-Lmid;

x = 1:dx:nx;
y = 1:dx:ny;

mask = zeros(length(x),length(y),'single');

for i=1:size(pos,1)
    [~,ix] = min(abs(x-pos(i,1)));
    [~,iy] = min(abs(y-pos(i,2)));
    mask(mod(arr+ix-1,nx)+1,mod(arr+iy-1,ny)+1) = mask(mod(arr+ix-1,nx)+1,mod(arr+iy-1,ny)+1) + cir;
end
mask = logical(mask);