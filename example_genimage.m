%setting up imaging conditions
params.C3   = 0; %don't change
params.C5   = 0; %don't change
params.C7   = 0; %don't change
params.df   = 0; %don't change
params.kev  = 200;
params.amin = 0; %don't change
params.amax = 10.5; %this can vary between 10.5 and 24
params.source = 0.8; %this can vary between 0.7 to 1.8
params.dwelltime = 16;
params.current = 5;
params.xgitter = 0.25; %angstrom
params.ygitter = params.xgitter;
N = 1024;
fov = 30;
angle = 10;
structname = 'struct_pt_110';
%ideal image
[II, M] = genimage(structname,params,fov,N,angle);
L = M.xlen;
dx = L/N;
%image with scan noise
IIshot = imAddShotNoise(II*0.1,params.dwelltime,params.current);
%image with scan noise
IIscan = imAddScanNoise(IIshot,params.xgitter/dx,params.ygitter/dx,params.dwelltime,0);

%convert to pixel coordiates. These are where the atoms are.
xpos = M.xpos/L*N + 1;
ypos = M.ypos/L*N + 1;

figure;
subplot(2,3,1)
imagesc(II); axis image; colormap(gray)
hold on;
plot(ypos,xpos,'r.');
subplot(2,3,2);
imagesc(IIshot); axis image; colormap(gray);
subplot(2,3,3);
imagesc(IIscan); axis image; colormap(gray);

%now add a randomly picke background
sel = ceil(rand(1)*252);
filename = fullfile(['D:\background'],['bg_' numstr_convert(sel,4) '.mat']);
load(filename);
II1 = IIscan./totmax(IIscan)+2*bg;
II2 = IIscan./totmax(IIscan)+0.8*bg;
II3 = IIscan./totmax(IIscan)+0.5*bg;
subplot(2,3,4);
imagesc(II1); axis image; colormap(gray);
subplot(2,3,5);
imagesc(II2); axis image; colormap(gray);
subplot(2,3,6);
imagesc(II3); axis image; colormap(gray);

imwrite(dbl28(II),'image.png');
imwrite(dbl28(IIshot),'image_shot.png');
imwrite(dbl28(IIscan),'image_scan.png');
imwrite(dbl28(II1),'image_bg1.png');
imwrite(dbl28(II2),'image_bg2.png');
imwrite(dbl28(II3),'image_bg3.png');