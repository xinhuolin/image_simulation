function M = NiO_110_xyz(nx,ny)
%function M = NiO_110_xyz(nx,ny)
%by Huolin Xin

nz = 1;
a =    5.1225;
b =    7.2440;
c =   2.9575;

pos = [

                   28  0.66666999999999998  0.16667000000000001                    1 
                   28  0.33333000000000002  0.50002000000000002                    0 
                   28                    1  0.83337000000000006                    0 
                   28                  0.5  0.83337000000000006                  0.5 
                   28  0.16667000000000001  0.16667000000000001                  0.5 
                   28  0.83333000000000002  0.50002000000000002                  0.5 
];

B = [8 28 %first row Znum
    0 0]; %second row B (A^2)

numatom = size(pos,1);
M.head = ['NiO [110] ' 'nx ' num2str(nx) ' ny ' num2str(ny) ' nz ' num2str(nz) ];
M.xpos = zeros(1,numatom*nx*ny*nz);
M.ypos = zeros(1,numatom*nx*ny*nz);
M.zpos = zeros(1,numatom*nx*ny*nz);
M.Znum = zeros(1,numatom*nx*ny*nz);
M.wt   = ones(1,numatom*nx*ny*nz);
M.tds  = zeros(1,numatom*nx*ny*nz);

M.xlen = a*nx;
M.ylen = b*ny;
M.zlen = c*nz;
num = 0;
for i=1:nz
    for j=1:nx
        for k=1:ny
            for l=1:numatom
                    num = num+1;
                    M.Znum(num) = pos(l,1);
                    M.xpos(num) = pos(l,2)*a+a*(j-1);
                    M.ypos(num) = pos(l,3)*b+b*(k-1);
                    M.zpos(num) = pos(l,4)*c+c*(i-1);
                    ind = find(B(1,:)==pos(l,1));
                    M.tds(num) = sqrt(B(2,ind)/3/(8/3*pi^2));
            end
        end
    end
end