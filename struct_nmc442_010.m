function M = NMC442_010_xyz(nx,ny)
%function M = NMC442_010_xyz(nx,ny,nz)
%by Huolin Xin

nz = 1;
a =    5.0313;
b =    14.0640;
c =   2.8662;

pos = [
                   25  0.68871000000000004 0.086180000000000007                   -0 
                   25  0.85096000000000005  0.25285000000000002                  0.5 
                   25 0.013220000000000001              0.41952                   -0 
                   25  0.33773999999999998  0.75287000000000004                   -0 
                   25              0.17548  0.58620000000000005                  0.5 
                   25                  0.5  0.91954000000000002                  0.5 

];

B = [3 25 8; %first row Znum
     0 0 0]; %second row B (A^2)

numatom = size(pos,1);
M.head = ['NiO [110] ' 'nx ' num2str(nx) ' ny ' num2str(ny) ' nz ' num2str(nz) ];
M.xpos = zeros(1,numatom*nx*ny*nz);
M.ypos = zeros(1,numatom*nx*ny*nz);
M.zpos = zeros(1,numatom*nx*ny*nz);
M.Znum = zeros(1,numatom*nx*ny*nz);
M.wt   = ones(1,numatom*nx*ny*nz);
M.tds  = zeros(1,numatom*nx*ny*nz);

M.xlen = a*nx;
M.ylen = b*ny;
M.zlen = c*nz;
num = 0;
for i=1:nz
    for j=1:nx
        for k=1:ny
            for l=1:numatom
                    num = num+1;
                    M.Znum(num) = pos(l,1);
                    M.xpos(num) = pos(l,2)*a+a*(j-1);
                    M.ypos(num) = pos(l,3)*b+b*(k-1);
                    M.zpos(num) = pos(l,4)*c+c*(i-1);
                    ind = find(B(1,:)==pos(l,1));
                    M.tds(num) = sqrt(B(2,ind)/3/(8/3*pi^2));
            end
        end
    end
end