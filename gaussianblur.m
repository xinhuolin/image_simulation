function [blur rr] = gaussianblur(L,N,fwhm,rmax)
%function [blur rr] = gaussianblur(L,N,fwhm,rmax)
%L is the dimension of the box in Angstrom
%N is how many pixels along one dimension
%fwhm is the FWHM of the gaussian blur in Angstrom
%rmax = crop radius or "auto"
%by Huolin Xin
%Copyright � 2013 Huolin Xin

ncenter = (floor(N/2)+1);
dx = L/N;
temp = ((1:N)-ncenter)*dx;
[xx,yy] = meshgrid(temp,temp);
rr = sqrt(xx.^2+yy.^2);
sigma = fwhm/2/sqrt(2*log(2));
if nargin == 4
    if isnumeric(rmax)
        n = ceil(rmax/dx);
    elseif isstr(rmax)
        n = ceil(2.5*sigma/dx);
    end
    rr = rr(ncenter-n:ncenter+n,ncenter-n:ncenter+n);
end
blur = exp(-rr.^2/(2*sigma*sigma))/2/pi/sigma^2;
blur = blur./sum(reshape(blur,1,[]));   
blur = ifftshift(blur);