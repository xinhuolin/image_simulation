function M = MoS2singlelayerxyz(nx,ny)
%function M = MoS2singlelayerxyz(nx,ny)
%by Huolin Xin

a =   3.12;
b =   sqrt(3)*a;
c =   1.54*2;
nz = 1;

pos = [
16 0 0 0.5
16 0 0 -0.5
42 0 2/3 0
42 0.5 1/6 0
16 0.5 0.5 0.5
16 0.5 0.5 -0.5
];

B = [16 42 %first row Znum
    0 0]; %second row B (A^2)

numatom = size(pos,1);
M.head = ['Single Layer of MoS2' 'nx ' num2str(nx) ' ny ' num2str(ny) ' nz ' num2str(nz) ];
M.xpos = zeros(1,numatom*nx*ny*nz);
M.ypos = zeros(1,numatom*nx*ny*nz);
M.zpos = zeros(1,numatom*nx*ny*nz);
M.Znum = zeros(1,numatom*nx*ny*nz);
M.wt   = ones(1,numatom*nx*ny*nz);
M.tds  = zeros(1,numatom*nx*ny*nz);

M.xlen = a*nx;
M.ylen = b*ny;
M.zlen = c*nz;
num = 0;
for i=1:nz
    for j=1:nx
        for k=1:ny
            for l=1:numatom
                    num = num+1;
                    M.Znum(num) = pos(l,1);
                    M.xpos(num) = pos(l,2)*a+a*(j-1);
                    M.ypos(num) = pos(l,3)*b+b*(k-1);
                    M.zpos(num) = pos(l,4)*c+c*(i-1);
                    ind = find(B(1,:)==pos(l,1));
                    M.tds(num) = sqrt(B(2,ind)/3/(8/3*pi^2));
            end
        end
    end
end
M.zpos = M.zpos-min(M.zpos);
