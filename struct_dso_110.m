function M = DSO_Pbnm_110_xyz(nx,ny)
%function M = DSO_Pbnm_110_xyz(nx,ny,nz)
%nx is how many unit cells along [001] axis (the c (7.9132 A) of DSO_Pbnm)
%ny is how many unit cells along the Ly axis (the Ly (7.8951 A) of DSO_Pbnm)
%nz is how many unit cells along the Lz [110] axis (the Lz (7.9048 A) of DSO_Pbnm)
%this script rotates the xyz file and truncates a portion with proper
%dimensions
%by Huolin Xin

nz = 1;
a =    7.913200; %the c of DSO
b =    5.726300; %the b of DSO
c =    5.449400; %the a of DSO

theta = atan(b/c); %the rotation angle. the angle between the [110] and [010]
Ly = c*sin(theta)+b*sin(pi/2-theta); %quasi-period in y
Lz = sqrt(c^2+b^2); %quasi-period in Z
l = ny; %store the y repetition in l
m = nz; %store the z repetition in m



pos =[ 
               21           1         0.000000   0.500000   0.000000    
               21           2         0.000000   1.000000   0.500000    
               21           3         0.500000   0.500000   0.000000    
               21           4         0.500000   1.000000   0.500000    
               66           1         0.250000   0.060700   0.017200    
               66           2         0.750000   0.439300   0.517200    
               66           3         0.750000   0.939300   0.982800    
               66           4         0.250000   0.560700   0.482800    ];

B = [66 21 8 %first row Znum
    0.35 0.7 1]; %second row B (A^2)

numatom = size(pos,1);
M.xpos = zeros(1,numatom*nx*ny*nz);
M.ypos = zeros(1,numatom*nx*ny*nz);
M.zpos = zeros(1,numatom*nx*ny*nz);
M.Znum = zeros(1,numatom*nx*ny*nz);
M.wt   = ones(1,numatom*nx*ny*nz);
M.tds  = zeros(1,numatom*nx*ny*nz);

M.xlen = a*nx;
M.ylen = b*ny;
M.zlen = c*nz;
num = 0;
for i=1:nz
    for j=1:nx
        for k=1:ny
            for h=1:numatom
                    num = num+1;
                    M.Znum(num) = pos(h,1);
                    M.xpos(num) = pos(h,3)*a+a*(j-1);
                    M.ypos(num) = pos(h,4)*b+b*(k-1);
                    M.zpos(num) = pos(h,5)*c+c*(i-1);
                    ind = (B(1,:)==pos(h,1));
                    M.tds(num) = sqrt(B(2,ind)/3/(8/3*pi^2));
            end
        end
    end
end

function flag = isodd(x)
%fucntion isodd(x)
%by Huolin Xin

index = (mod(x,2)==0);
flag = ones(size(x));
flag(index) = 0;