function [II M] = genimage(structname,params,fov,N,angle)
%structname = e.g. 'struct_mos2'
%fov = field of view in angstrom
%N = number of pixel (NxN)
%angle = rotation angle in degree
%by Huolin Xin

%construct inline function
g = inline([structname,'(x,y)'],'x','y');
%generate a 1x1 structure to extract the unit cell size
test = g(1,1);
a = test.xlen;
b = test.ylen;
%calculate the tile repetition
nx = round(fov/a);
ny = round(fov/b);

if nx<1
    nx = 1;
end
if ny<1
    ny = 1;
end

M = g(nx,ny);
xlen = M.xlen;
ylen = M.ylen;
L = max(xlen,ylen);
rmat = rotatemat('z', angle/180*pi);
temp = rmat*[M.xpos - M.xlen/2;M.ypos - M.ylen/2;M.zpos];
for i = 1:2
    temp(i,:) = temp(i,:) + L/2;
end
ind = temp(1,:)>=0 & temp(1,:)<L & temp(2,:)>=0 & temp(2,:)<L;
M.xpos = temp(1,ind);
M.ypos = temp(2,ind);
M.zpos = temp(3,ind);
M.Znum = M.Znum(ind);
M.xlen = L;
M.ylen = L;
%calculate the potential map
V = potmap(L,L,N,N,M);
dx = L/N;

%generate the point spread function
probe = probeblur(L,N,params);
blur = gaussianblur(L,N,params.source);
ctf = fft2(probe).*fft2(blur);

%final image;
II = ifft2(fft2(V).*ctf,'symmetric');
II = II/max(II(:));