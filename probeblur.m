function blur = probeblur(L,N,params,rmax)
%function blur = probeblur(L,N,params,rmax)
%by Huolin Xin

probe = probe_mul(L,N,0,0,params);
if nargin == 3
    blur = probe.*conj(probe);
else
    probe = fftshift(probe);
    ncenter = (floor(N/2)+1);
    dx = L/N;
    n = ceil(rmax/dx);
    probe = probe(ncenter-n:ncenter+n,ncenter-n:ncenter+n);
    blur = probe.*conj(probe);
    blur = blur./sum(sum(blur));
end