function M = TMO_001_xyz(nx,ny,a)
%function M = TMO_001_xyz(nx,ny,a)
%genralized fcc structure along 001
%a = [a b c] or [a]
%Z = [Z1 Z2 Z3 Z4] or [Z]
%Z1 = z number of the TM atom 1
%Z2 = z number of the TM atom 2 
%by Huolin Xin
%copyrighted 12/15/2012
%� 2012 Huolin Xin

Z = 27;
nz = 1;
a = 4.18;
%% arguments preprocessing
if numel(a)==1
    b =   a;
    c =   a;
else
    b = a(2);
    c = a(3);
    a = a(1);
end

if numel(Z)==1
    Z1 = Z;
    Z2 = Z;
else
    Z1 = Z(1);
    Z2 = Z(2);
end


pos = [
Z1 0.0 0.0 0.0
Z2 0.5 0.5 0
Z2 0.5 0 0.5
Z1 0 0.5 0.5
];


%% makeing the crystal
numatom = size(pos,1);
M.head = ['Pt structure along [001] ' 'nx ' num2str(nx) ' ny ' num2str(ny) ' nz ' num2str(nz)];
M.xpos = zeros(1,numatom*nx*ny*nz);
M.ypos = zeros(1,numatom*nx*ny*nz);
M.zpos = zeros(1,numatom*nx*ny*nz);
M.Znum = zeros(1,numatom*nx*ny*nz);
M.wt   = ones(1,numatom*nx*ny*nz);
M.tds  = zeros(1,numatom*nx*ny*nz);

M.xlen = a*nx;
M.ylen = b*ny;
M.zlen = c*nz;
num = 0;
for i=1:nz
    for j=1:nx
        for k=1:ny
            for l=1:numatom
                    num = num+1;
                    M.Znum(num) = pos(l,1);
                    M.xpos(num) = pos(l,2)*a+a*(j-1);
                    M.ypos(num) = pos(l,4)*b+b*(k-1);
                    M.zpos(num) = pos(l,3)*c+c*(i-1);
            end
        end
    end
end
