function M = si110xyz(nx,ny)
%original code was wrong on multiple points. Author of the original code:
%unknown;
%rewritten by Huolin Xin on 4/4/2007

nz = 1;
TDS = 0;

a = 5.4300; % to accomodate the xyz file of a-si c-si a-si

x = [0 a/sqrt(2)/2 0 a/sqrt(2)/2]; %ax = a/sqrt(2)
y = [0 a*3/4 a/4 a/2]; %ay = a
z = [0 0 a/2/sqrt(2) a/2/sqrt(2)];

xperiod = a/sqrt(2);
yperiod = a;
zperiod = a/sqrt(2);

M.head = ['Si [110] ' 'nx ' num2str(nx) ' ny ' num2str(ny) ' nz ' num2str(nz) ];
M.xpos = zeros(1,4*nx*ny*nz);
M.ypos = zeros(1,4*nx*ny*nz);
M.zpos = zeros(1,4*nx*ny*nz);
M.Znum = 14*ones(1,4*nx*ny*nz);
M.wt   = ones(1,4*nx*ny*nz);
if nargin==3
    M.tds  = zeros(1,4*nx*ny*nz);
elseif nargin==4
    if TDS==1
        M.tds  = zeros(1,4*nx*ny*nz)+0.076;
    else
        M.tds  = zeros(1,4*nx*ny*nz);
    end 
end
M.xlen = xperiod*nx;
M.ylen = yperiod*ny;
M.zlen = zperiod*nz;

num = 0;
for k = 0:nz-1
    for i = 0:nx-1
        for j = 0:ny-1      
            for l = 1:4
                num = num+1;
                M.xpos(num) = x(l)+xperiod*i;
                M.ypos(num) = y(l)+yperiod*j;
                M.zpos(num) = z(l)+zperiod*k;
            end
        end
    end
end



% function M = si110xyz(nx,ny,nz)
% 
% M.head = ['Si [110]' num2str(nx) 'x' num2str(ny) 'x' num2str(nz) ];
% 
% acell = 5.43295;   % lattice constant of silicon
% Z=14;
% wt=1;
% %projected unit cell along [110]
% xp= acell*[ 0, 0,  sqrt(2)/4, sqrt(2)/4];
% yp= acell*[ 0, 1/4, 1/2,           3/4];
% zp= acell*[ 0, 0,    0,           0];
% 
% numat = length(xp);
% 
% %size of the projected unit cell
% ax= acell/sqrt(2);
% ay= acell;
% az= acell/2;
% 
% M.xlen = ax*nx;
% M.ylen = ay*ny;
% M.zlen = az*nz;
% 
% M.Znum = Z*ones(1, numat*nx*ny*nz);
% M.wt   = wt*ones(1,numat*nx*ny*nz);
% M.tds = zeros(1,numat*nx*ny*nz);
% 
% M.xpos = zeros(1,numat);
% M.ypos = zeros(1,numat);
% M.zpos = zeros(1,numat);
% 
% n=1;
% for iz= 0:nz-1,
%     for ix = 0:nx-1,
%         for iy = 0:ny-1,
%             for l=1:numat,
%                 M.xpos(n) = ax*ix+xp(l);
%                 M.ypos(n) = ay*iy+yp(l);
%                 M.zpos(n) = az*iz+zp(l);
%                 n=n+1;
%             end;
%         end;
%     end;
% end;