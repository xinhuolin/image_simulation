function M = struct_fcc_110(nx,ny)
%function M = fcc_110_xyz(nx,ny,nz,a)
%genralized fcc structure along 001
%a = lattice constant
%Z = atomic number (Z number)
%B = [B]
%B = the debye waller factor
%by Huolin Xin
%copyrighted 12/15/2012
%� 2012 Huolin Xin

%% arguments preprocessing
nz = 1;
Z = 27;
acell = 3.92;
a = acell*sqrt(2)/2;
b = acell;
c = acell*sqrt(2)/2;

if nargin < 6
    B = 0;
end


pos = [
Z 0 0 0
Z 0.5 0.5 0.5
];

B = [Z %first row Znum
    B ]; %second row B (A^2)

%% makeing the crystal
numatom = size(pos,1);
M.head = ['Pt structure along [001] ' 'nx ' num2str(nx) ' ny ' num2str(ny) ' nz ' num2str(nz)];
M.xpos = zeros(1,numatom*nx*ny*nz);
M.ypos = zeros(1,numatom*nx*ny*nz);
M.zpos = zeros(1,numatom*nx*ny*nz);
M.Znum = zeros(1,numatom*nx*ny*nz);
M.wt   = ones(1,numatom*nx*ny*nz);
M.tds  = zeros(1,numatom*nx*ny*nz);

M.xlen = a*nx;
M.ylen = b*ny;
M.zlen = c*nz;
num = 0;
for i=1:nz
    for j=1:nx
        for k=1:ny
            for l=1:numatom
                    num = num+1;
                    M.Znum(num) = pos(l,1);
                    M.xpos(num) = pos(l,2)*a+a*(j-1);
                    M.ypos(num) = pos(l,4)*b+b*(k-1);
                    M.zpos(num) = pos(l,3)*c+c*(i-1);
                    ind = find(B(1,:)==pos(l,1));
                    ind = ind(1);
                    M.tds(num) = sqrt(B(2,ind)/3/(8/3*pi^2));
            end
        end
    end
end
