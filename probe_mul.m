function probe = probe_mul(L,N,posx,posy,params)
%function probe = probe_mul(L,N,posx,posy,params)
%by Huolin Xin
% Copyright � 2013 Huolin Xin


C3 = params.C3*1.0e7;
C5 = params.C5*1.0e7;
C7 = params.C7*1.0e7;
kev = params.kev;
amax = params.amax*0.001;
amin = params.amin*0.001;
wav = 12.3986./sqrt((2*511.0+kev).*kev);
df = params.df;
klimitmax = amax/wav;
klimitmin = amin/wav;
Nz = length(df);

dk = 1/L;
temp = (1:N)-(floor(N/2)+1);%-N/2:(N/2-1);
[kxx,kyy] = meshgrid(temp,temp);
kxx = kxx.*dk;
kyy = kyy.*dk;
kr2 = kxx.^2+kyy.^2;
kr = sqrt(kr2);
%chi = -pi*wav*kr2*df + pi/2*C3*wav^3*kr2.^2+pi/3*C5*wav^5*kr2.^3+pi/4*C7*wav^7*kr2.^4 + 2*pi*(kxx.*posx + kyy.*posy);
chiab = pi/2*C3*wav^3*kr2.^2+pi/3*C5*wav^5*kr2.^3+pi/4*C7*wav^7*kr2.^4 + 2*pi*(kxx.*posx + kyy.*posy);
chi = zeros(N,N,Nz);
for i=1:Nz
    chi(:,:,i) = -pi*wav*kr2*df(i) + chiab;
end
clear chiab;
phase = exp(sqrt(-1).*chi);
clear chi;
mask = double(kr<=klimitmax).*double(kr>=klimitmin);
for i=1:Nz
    phase(:,:,i) = ifftshift(phase(:,:,i).*mask);
end
clear mask kxx kyy kr kr2;

% figure;
% imagesc(abs(phase));
% phase = fftshift(phase);
%plot3dmat(abs(phase));
probe = fft2(phase);
probe = probe/sqrt(sum(sum(probe.*conj(probe))));
%probe = probe/(L/N)^2; %normalize the probe. Because in reciprocal space
%the zero frequency amplitute is 1, so, the summation is also one.